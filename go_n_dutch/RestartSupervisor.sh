#!/bin/bash
USERNAME=ubuntu
PRIVATE_KEY_PATH='/home/barkha/Desktop/keys/go_n_dutch.pem'

# ADMIN
HOSTS="54.153.50.112"


SCRIPT="sudo supervisorctl restart GENERAL; sudo supervisorctl restart FRIEND_REQUEST; sudo supervisorctl restart RESTAURANT_BOOKING; sudo supervisorctl restart WELCOME_MESSAGE; sudo supervisorctl restart FRIEND_REQUEST_SEND; sudo supervisorctl restart FRIEND_REQUEST_ACCEPT; sudo supervisorctl restart FRIEND_REQUEST_REJECT; sudo supervisorctl restart BOOKING_REQUEST_ACCEPT; sudo supervisorctl restart BOOKING_REQUEST_REJECT;"

i=0
for HOSTNAME in ${HOSTS} ; do
    i=`expr $i + 1`;
    echo "----------------$i----------------------";
    ssh -i ${PRIVATE_KEY_PATH} ${USERNAME}@${HOSTNAME} "${SCRIPT}"
done
