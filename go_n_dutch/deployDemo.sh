#!/bin/bash
USERNAME=ubuntu
GIT_USER=barkha156
GIT_PWD=argus156
PRIVATE_KEY_PATH='/home/barkha/Desktop/keys/go_n_dutch.pem'


# ADMIN
HOSTS="54.153.50.112"

SCRIPT="cd /home/ubuntu/go_n_dutch/go_n_dutch_admin; git pull 'https://${GIT_USER}:${GIT_PWD}@bitbucket.org/SeashorePartners/go_n_dutch_admin.git' develop; source ../env/bin/activate; pip install -r requirements.txt; python manage.py migrate; python manage.py collectstatic --noinput; sudo supervisorctl restart GonDutchAdmin;"

i=0
for HOSTNAME in ${HOSTS} ; do
    i=`expr $i + 1`;
    echo "----------------$i----------------------";
   ssh -i ${PRIVATE_KEY_PATH} ${USERNAME}@${HOSTNAME} "${SCRIPT}"
done
