#!/bin/bash
USERNAME=ubuntu
PRIVATE_KEY_PATH='/home/barkha/Desktop/keys/adyoyo_pvkey.pem'

# ADMIN
HOSTS="54.242.168.163"

SCRIPT="sudo supervisorctl restart ADVERTISEMENT_BLOCKED_BY_ADMIN; sudo supervisorctl restart ADVERTISEMENT_BLOCKED_BY_ADMIN_PUSH_NOTIFICATION; sudo supervisorctl restart ADVERTISEMENT_POSTED; sudo supervisorctl restart ADVERTISEMENT_POSTED_PUSH_NOTIFICATION; sudo supervisorctl restart AFTER_RESET_PASSWORD; sudo supervisorctl restart AFTER_RESET_PASSWORD_PUSH_NOTIFICATION; sudo supervisorctl restart EMAIL_VERIFICATION; sudo supervisorctl restart EMAIL_VERIFICATION_PUSH_NOTIFICATION; sudo supervisorctl restart IMAGES_UPLOAD; sudo supervisorctl restart RESET_FORGOT_PASSWORD; sudo supervisorctl restart USER_ACCOUNT_BLOCKED_BY_ADMIN; sudo supervisorctl restart USER_ACCOUNT_BLOCKED_BY_ADMIN_PUSH_NOTIFICATION; sudo supervisorctl restart WELCOME_EMAIL; sudo supervisorctl restart OFFLINE_CHAT_MESSAGE; sudo supervisorctl restart OFFLINE_CHAT_PUSH_NOTIFICATION;"

i=0
for HOSTNAME in ${HOSTS} ; do
    i=`expr $i + 1`;
    echo "----------------$i----------------------";
    ssh -i ${PRIVATE_KEY_PATH} ${USERNAME}@${HOSTNAME} "${SCRIPT}"
done
