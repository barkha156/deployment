#!/bin/bash
USERNAME=ubuntu
GIT_USER=SeashorePartners
GIT_PWD=A26424801
PRIVATE_KEY_PATH='/Users/idream/iDrive/Projects/Adyoyo/AWS/adyoyo_pvkey.pem'

# WEB
HOSTS="54.144.121.43 54.173.220.105"

SCRIPT="cd /home/ubuntu/adyoyobeta/adyoyo_web; git pull 'https://${GIT_USER}:${GIT_PWD}@bitbucket.org/SeashorePartners/adyoyo_web.git' release; source ../env/bin/activate; python manage.py collectstatic --noinput --settings=adyoyo_web.settings.com; sudo supervisorctl restart Adyoyo;"

i=0
for HOSTNAME in ${HOSTS} ; do
    i=`expr $i + 1`;
    echo "----------------$i----------------------";
    ssh -i ${PRIVATE_KEY_PATH} ${USERNAME}@${HOSTNAME} "${SCRIPT}"
done
