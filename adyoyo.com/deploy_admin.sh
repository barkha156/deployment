#!/bin/bash
USERNAME=ubuntu
GIT_USER=SeashorePartners
GIT_PWD=A26424801
PRIVATE_KEY_PATH='/Users/idream/iDrive/Projects/Adyoyo/AWS/adyoyo_pvkey.pem'


# ADMIN
HOSTS="34.207.22.70 52.45.153.147"

SCRIPT="cd /home/ubuntu/adyoyobeta/adyoyo_admin; git pull 'https://${GIT_USER}:${GIT_PWD}@bitbucket.org/SeashorePartners/adyoyo_admin.git' release; sudo supervisorctl restart AdyoyoAdminPanel;"

i=0
for HOSTNAME in ${HOSTS} ; do
    i=`expr $i + 1`;
    echo "----------------$i----------------------";
   ssh -i ${PRIVATE_KEY_PATH} ${USERNAME}@${HOSTNAME} "${SCRIPT}"
done


SCRIPT="cd /home/ubuntu/adyoyobeta/adyoyo_admin; source ../env/bin/activate; python manage.py collectstatic --noinput --settings=adyoyo_admin.settings.com;"
echo "Adding static content to s3 bucket."
ssh -i ${PRIVATE_KEY_PATH} ${USERNAME}@34.207.22.70 "${SCRIPT}"
