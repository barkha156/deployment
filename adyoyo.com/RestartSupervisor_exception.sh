#!/bin/bash
USERNAME=ubuntu
PRIVATE_KEY_PATH='/Users/idream/iDrive/Projects/Adyoyo/AWS/adyoyo_pvkey.pem'

# ADMIN
HOSTS="34.207.22.70 52.45.153.147"
SCRIPT="sudo supervisorctl restart ADMIN_ERROR_LOG;"

i=0
for HOSTNAME in ${HOSTS} ; do
    i=`expr $i + 1`;
    echo "----------------$i----------------------";
    ssh -i ${PRIVATE_KEY_PATH} ${USERNAME}@${HOSTNAME} "${SCRIPT}"
done
