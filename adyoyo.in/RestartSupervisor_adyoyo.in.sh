#!/bin/bash
USERNAME=ubuntu
PRIVATE_KEY_PATH='/home/barkha/Desktop/adyoyo_inkey.pem'

# ADMIN
HOSTS="13.126.85.249 13.126.22.18"
SCRIPT="sudo supervisorctl restart AFTER_RESET_PASSWORD; sudo supervisorctl restart AFTER_RESET_PASSWORD_PUSH_NOTIFICATION; sudo supervisorctl restart ALL_ANDROID_USER; sudo supervisorctl restart ALL_IOS_USER; sudo supervisorctl restart EMAIL_VERIFICATION; sudo supervisorctl restart EMAIL_VERIFICATION_PUSH_NOTIFICATION; sudo supervisorctl restart HIGHER_OFFER_PLACED; sudo supervisorctl restart HIGHER_OFFER_PLACED_PUSH_NOTIFICATION; sudo supervisorctl restart HOW_TO_VIDEO_BLOCKED_BY_ADMIN; sudo supervisorctl restart HOW_TO_VIDEO_BLOCKED_BY_ADMIN_PUSH_NOTIFICATION; sudo supervisorctl restart HOW_TO_VIDEO; sudo supervisorctl restart HOW_TO_VIDEO_POSTED; sudo supervisorctl restart HOW_TO_VIDEO_POSTED_PUSH_NOTIFICATION; sudo supervisorctl restart OFFER_ACCEPTED; sudo supervisorctl restart OFFER_ACCEPTED_PUSH_NOTIFICATION; sudo supervisorctl restart OFFER_REJECTED; sudo supervisorctl restart OFFER_REJECTED_PUSH_NOTIFICATION; sudo supervisorctl restart PRODUCT_BLOCKED_BY_ADMIN; sudo supervisorctl restart PRODUCT_BLOCKED_BY_ADMIN_PUSH_NOTIFICATION; sudo supervisorctl restart PRODUCT_OFFER_PLACED; sudo supervisorctl restart PRODUCT_OFFER_PLACED_PUSH_NOTIFICATION; sudo supervisorctl restart PRODUCT_POSTED; sudo supervisorctl restart PRODUCT_POSTED_PUSH_NOTIFICATION; sudo supervisorctl restart PRODUCT_SOLD; sudo supervisorctl restart PRODUCT_SOLD_PUSH_NOTIFICATION; sudo supervisorctl restart PRODUCT_VIDEO; sudo supervisorctl restart RESET_FORGOT_PASSWORD; sudo supervisorctl restart SELLER_POST_NEW_ITEM; sudo supervisorctl restart SELLER_POST_NEW_ITEM_PUSH_NOTIFICATION; sudo supervisorctl restart UPLOAD_FAILED_VIDEO; sudo supervisorctl restart USER_ACCOUNT_BLOCKED_BY_ADMIN; sudo supervisorctl restart USER_ACCOUNT_BLOCKED_BY_ADMIN_PUSH_NOTIFICATION; sudo supervisorctl restart VIDEO_META; sudo supervisorctl restart VIDEO_PROCESS; sudo supervisorctl restart WELCOME_EMAIL; sudo supervisorctl restart FOLLOWER_SELLER_PUSH_NOTIFICATION; sudo supervisorctl restart UNFOLLOW_SELLER_PUSH_NOTIFICATION; sudo supervisorctl restart OFFLINE_CHAT_MESSAGE; sudo supervisorctl restart PRODUCT_EXPIRED; sudo supervisorctl restart PRODUCT_EXPIRED_PUSH_NOTIFICATION; sudo supervisorctl restart KALTURA_VIDEO_STATUS; sudo supervisorctl restart ADD_WATERMARK_TO_VIDEO; sudo supervisorctl restart ADMIN_ERROR_LOG; sudo supervisorctl restart CELERY_CAM;"

i=0
for HOSTNAME in ${HOSTS} ; do
    i=`expr $i + 1`;
    echo "----------------$i----------------------";
    ssh -i ${PRIVATE_KEY_PATH} ${USERNAME}@${HOSTNAME} "${SCRIPT}"
done
