#!/bin/bash
USERNAME=root
GIT_USER=barkha156
GIT_PWD=argus156


# ADMIN
HOSTS="159.65.114.187"

SCRIPT="cd /root/flatfit/flatfit_admin; git pull 'https://${GIT_USER}:${GIT_PWD}@bitbucket.org/SeashorePartners/flatfit_admin.git' develop; source ../envFlatfit/bin/activate; python manage.py collectstatic --noinput; cd ngApp; npm install; tsc; sudo supervisorctl restart FlatfitAdmin;"

echo "Deploying Admin Flatfit...."
i=0
for HOSTNAME in ${HOSTS} ; do
    i=`expr $i + 1`;
    echo "----------------$i----------------------";
   ssh ${USERNAME}@${HOSTNAME} "${SCRIPT}"
done


SCRIPT="cd /root/flatfit/flatfit_web; git pull 'https://${GIT_USER}:${GIT_PWD}@bitbucket.org/SeashorePartners/flatfit_web.git' develop; npm install; ng build;"


echo "Deploying Web Flatfit...."
i=0
for HOSTNAME in ${HOSTS} ; do
    i=`expr $i + 1`;
    echo "----------------$i----------------------";
   ssh ${USERNAME}@${HOSTNAME} "${SCRIPT}"
done
